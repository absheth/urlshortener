class Url < ActiveRecord::Base
	validates :long_url,  presence: true
	URL_CHARS	= 	"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	BASE		=	URL_CHARS.length
	def getShortUrl
		id_url	= self.id
		output	=	""
		while id_url > 0 do
			remainder = id_url%BASE
			output << URL_CHARS[remainder]
			id_url/=BASE
		end
		#debugger
		output.reverse
	end
end
