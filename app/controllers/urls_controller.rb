class UrlsController < ApplicationController
	def index
		short_url	=	params["id"]
		url 	=	Url.find_by(short_url: short_url)
		if url
			l_long_url = url.long_url
      puts l_long_url
      if l_long_url.index("http") !=0 
        redirect_to "http://#{l_long_url}"
      else
        redirect_to l_long_url
      end
		end
	end
	def new
  		@new_url = Url.new
 	end

	def create
		long_url = params["url"]["long_url"]
    @new_url = Url.find_by(long_url: long_url)
  	if @new_url
  		return @new_url
  	else
  		@new_url = Url.new(long_url: long_url)
  		if @new_url.save
        short_url = @new_url.getShortUrl
  		  @new_url.update_attribute(:short_url, short_url)
      else
        render 'new'
      end
  	end
  	@new_url
  end
	private
		def url_params
			params.require(:url).permit( :long_url )
		end
end
